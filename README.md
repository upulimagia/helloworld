# HelloWorld

**Note**: This repository contians some artifacts that are necessary for running Federated Learning server's test cases.

For more details regarding setting up this repository for running FL test cases, please refer to the README.md file of the FL server.  

## Installation

To guarenties a smooth working environment, it is recommanded to use virtual envirnoment:

```
virtualenv .env
source .env/bin/activate
```


then install the project and it's dependancies:
```
sudo $(which pip) install -e .
sudo $(which pip) install -r requirements.txt
```

The following environment variables also need to be defined:
```
export LC_ALL=C.UTF-8
export LANG=C.UTF-8
```

Alternatively, you can run the installation script by doing `source install.sh`.

## Training
For now, a config file needs to be defined to run the experiments. An exemple can be found in `config/mnist.yml`
To run the code, simply launch
```
ai.helloworld train --config ai/helloworld/config/mnist.yml
```

## Monitoring
The code right now will log all experiments in the `logs/experiments.csv` file. The time of saving, git hash, config, and best accuracy on the valid set is saved.

## Adding my own model and dataset
To add your new fancy model/dataset that is not already in tensorflow, here are the steps to follow:
1. Add you class in `models/datasets` either in a new file or a preexisting one.
3. Register the model/dataset/callback with the appropriate function (ex: `@register.setmodelname("cnn_example")`)

## Bayesian hyperparameters optimization with skopt
Steps to launch bayesian hyperparameters search:
1. In your `.yml` config file, choose the parameters you want to optimize (i.e. learning rate).
2. Replace the value with  the search parameters. For example:
    ```
    # Optimizer
    optimizer:
      Adam:
        lr: "!skopt Real(10**-4, 10**-2, 'log-uniform', name='lr')"
    ```
    search the learning rate in the range (0.01, 0.0001), on a log scale. For more examples, please refer to [the official documentation](https://scikit-optimize.github.io/#skopt.BayesSearchCV).
3. Launch your config file with `iia.helloworld train-skopt --config your/config/path.yml`. A config example can be found in `config/mnist_skopt.yml`
### Shared hyperparameters
It is often the case that the same hyperparameter is used at multiple different places (for example the number of filters). To only optimize it once, you can do as follow:
```
# Model
model:
  CNN_1:
    filters_param: "!skopt Integer(8, 128, name='filter')"
   ...
  filters_param_2: "!skopt 'filter'"
  ...
  filters_param_3: "!skopt 'filter'"
```

This way, the hyper parameter `filter` will only be sample once and replace everywhere in the configuration file.



