#!/usr/bin/env python
import os
from setuptools import setup

version = os.environ.get('VERSION', '0.0.0')

setup(
    name='ai.helloworld',
    description='Simple research project example.',
    version=version,
    author='OI Template',
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'ai.helloworld = ai.helloworld.main:main'
        ]
    }

)
