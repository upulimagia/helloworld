from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D

import ai.helloworld.utils.register as register


# model taken from: https://github.com/keras-team/keras/blob/master/examples/mnist_cnn.py
# Ge register the model under the name "cnn_example". Simply use that in the "model" section of the config file.
@register.setmodelname("mlp_example")
def model(nb_channel=32, nb_class=10, dropout=0.5):
    model = Sequential([
        Flatten(input_shape=(28, 28, 1)),
        Dense(128, activation='relu'),
        Dropout(0.2),
        Dense(10, activation='softmax')
    ])
    return model
