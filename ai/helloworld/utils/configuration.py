import importlib
import inspect
import logging
import yaml
import functools
import ai.helloworld.models as models
import ai.helloworld.datasets as datasets
import ai.helloworld.callbacks as cb

_LOG = logging.getLogger(__name__)


def load_config(config_file):

    default_config = {'cuda': True,
                      'seed': 1234
    }

    with open(config_file, 'r') as f:
        yaml_cfg = yaml.load(f, Loader=yaml.Loader)

    return {**default_config, **yaml_cfg}


def get_available_classes(mod, mod_path, control_variable):
    """
    Get all classes objects available in a custom module

    :param mod: the module
    :type mod: object
    :param mod_path: path to the module
    :type mod_path: str
    :param control_variable: module specific attribute name (please refer to the documentation sec XX)
    :type control_variable: str
    :return: a dictionary with the associated class objects
    :rtype: dict{str: object}
    """
    available_objects = {}
    for c in mod.__all__:
        m = importlib.import_module(mod_path + c)
        for name, obj in inspect.getmembers(m, lambda x: inspect.isclass(x) or inspect.isfunction(x)):

            if control_variable not in obj.__dict__:
                continue

            available_objects[obj.__dict__[control_variable]] = obj
    return available_objects

def setup_model(config, yaml_section='model'):
    """
    Prepare model according to config file.
    """

    available_models = get_available_classes(models, 'ai.helloworld.models.', '_MODEL_NAME')
    models_from_module = importlib.import_module('tensorflow.keras.applications')
    

    if type(yaml_section) == str and yaml_section != '':
        yaml_section = [yaml_section]
    sub_section = functools.reduce(lambda sub_dict, key: sub_dict.get(key), yaml_section, config)
    model_name = list(sub_section.keys())[0]
    model_args = list(sub_section.values())[0]

    _LOG.info('Model {} with arguments {}'.format(model_name, model_args))

    if hasattr(models_from_module , model_name):
        obj = getattr(models_from_module, model_name)
    else:
        obj = available_models[model_name]

    # Create the model
    model = obj(**model_args)
    return model


def setup_dataset(config, split='train'):
    """
    Prepare data generators for training set and optionally for validation set.
    """

    available_datasets = get_available_classes(datasets, 'ai.helloworld.datasets.', '_DG_NAME')
    datasets_from_module = importlib.import_module('tensorflow.keras.datasets')

    dataset_name = list(config['dataset'][split].keys())[0]
    dataset_args = list(config['dataset'][split].values())[0]

    _LOG.info('Dataset {} with arguments {}'.format(dataset_name, dataset_args))
    if hasattr(datasets_from_module , dataset_name):
        generator = getattr(datasets_from_module, dataset_name)
    else:
        generator = available_datasets[dataset_name]

    dataset = generator(**dataset_args)
    return dataset

def setup_optimizer(config, yaml_section='optimizer'):
    """
    Prepare optimizer according to configuration file.
    """

    optimizer_module = importlib.import_module('tensorflow.python.keras.optimizers')

    if type(yaml_section) == str and yaml_section != '':
        yaml_section = [yaml_section]
    sub_section = functools.reduce(lambda sub_dict, key: sub_dict.get(key), yaml_section, config)
    optimizer_name = list(sub_section.keys())[0]
    optimizer_args = list(sub_section.values())[0]

    _LOG.info('Optimizer {} with arguments {}'.format(optimizer_name, optimizer_args))

    optimizer_obj = getattr(optimizer_module, optimizer_name)
    obj = optimizer_obj(**optimizer_args)

    return obj

def setup_callbacks(config, yaml_section='callbacks'):
    """
    Prepare callback(s) according to configuration file.
    """

    available_callbacks = get_available_classes(cb, 'ai.helloworld.callbacks.', '_CB_NAME')
    callbacks = []
    callback_module = importlib.import_module('tensorflow.python.keras.callbacks')

    if yaml_section in config:
        for c in config[yaml_section]:

            cb_name, cb_args = list(c.keys())[0], list(c.values())[0]

            if hasattr(callback_module, cb_name):
                callback = getattr(callback_module, cb_name)
                callbacks += [callback(**cb_args)]
            else:
                callback_obj = available_callbacks[cb_name]
                callbacks += [callback_obj(**cb_args)]

    return callbacks

