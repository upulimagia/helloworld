import numpy as np
import logging
import random
from collections import OrderedDict
import copy
import ai.helloworld.utils.configuration as configuration

import tensorflow as tf
from tensorflow.python.keras import backend as K

_LOG = logging.getLogger(__name__)


def train(cfg):

    print("Our config:", cfg)
    seed = cfg['seed']
    num_epoch = cfg['epoch']

    # Setting the seed.
    np.random.seed(seed)
    random.seed(seed)
    tf.set_random_seed(seed)

    # Session
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    K.set_session(tf.Session(config=config))


    # Dataset
    train_generator = configuration.setup_dataset(cfg, 'train')
    valid_generator = configuration.setup_dataset(cfg, 'valid')

    # Model
    model = configuration.setup_model(cfg)
    model.summary()

    # Optimizer
    optimizer = configuration.setup_optimizer(cfg)

    # Callback
    callbacks = configuration.setup_callbacks(cfg)

    # Log metadata information (run time, memory usage, etc.)
    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    run_metadata = tf.RunMetadata()

    # Compilation
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'],
                  run_metadata=run_metadata,
                  options=run_options)

    # Training
    history = model.fit_generator(generator=train_generator,
                                  epochs=num_epoch,
                                  validation_data=valid_generator,
                                  callbacks=callbacks)
    #history = model.predict_generator(generator=valid_generator)

    best_metric = np.max(np.array(history.history['val_acc']))

    K.clear_session()
    return best_metric
    

def train_skopt(cfg, n_iter, base_estimator, n_initial_points, random_state, train_function=train):

    """
    Do a Bayesian hyperparameter optimization.

    :param cfg: Configuration file.
    :param n_iter: Number of Bayesien optimization steps.
    :param base_estimator: skopt Optimization procedure.
    :param n_initial_points: Number of random search before starting the optimization.
    :param random_state: seed.
    :param train_function: The trainig procedure to optimize. The function should take a dict as input and return a metric maximize.
    :return:
    """

    import skopt
    import yaml
    from skopt.space import Real, Integer, Categorical

    def recursive_update(to_update, config):

        from skopt.space import Real, Integer, Categorical

        new_config = copy.deepcopy(config)
        for key, value in new_config.items():

            if isinstance(value, dict):
                new_config[key] = recursive_update(to_update, value)

            if isinstance(value, str) and '!skopt' in value:
                this_op = value.split('!skopt')[-1]

                try:
                    update_key = eval(this_op)._name
                except:
                    update_key = this_op

                new_config[key] = to_update[update_key]

        return new_config

    def recursive_extract(config):

        skopt_ops = {}

        def inner_extract(sub_config):

            new_config = copy.deepcopy(config)
            for key, value in sub_config.items():

                if isinstance(value, dict):
                    inner_extract(value)

                if isinstance(value, str) and '!skopt' in value:
                    this_op = value.split("!skopt")[-1]
                    try:
                        x = eval(this_op)
                        if isinstance(x, (Real, Integer, Categorical)):
                            skopt_ops[x._name] = x

                    except:
                        pass

        inner_extract(config)
        return skopt_ops

    # Extract the space
    shared_skopt = recursive_extract(cfg)

    # Create the optimizer
    optimizer = skopt.Optimizer(dimensions=shared_skopt.values(),
                                base_estimator=base_estimator,
                                n_initial_points=n_initial_points,
                                random_state=random_state)

    # Do a bunch of loops.
    for _ in range(n_iter):

        suggestion = optimizer.ask()
        suggestion_tmp = {k: v for k, v in zip(shared_skopt.keys(), suggestion)}
        this_cfg = recursive_update(suggestion_tmp, cfg)

        try:
            optimizer.tell(suggestion, - train_function(this_cfg)) # We minimize the negative accuracy/AUC
        except RuntimeError as e:
            print("The following error was raised:\n {} \n, launching next experiment.".format(e))
            optimizer.tell(suggestion, 0.)  # Something went wrong, (probably a CUDA error).

    # Done! Hyperparameters tuning has never been this easy.
