import numpy as np
import importlib
import tensorflow.keras as K
import ai.helloworld.utils.register as register

# Wrapper class to make everything fit in the dataGenerator framework.
# Take any basic module in parameters (mnist, cifar10, etc.) and convert it in the datagenerator.
@register.setdatasetname("GenericDataGenerator")
class GenericDataGenerator(K.utils.Sequence):

    def __init__(self, data_module, n_classes, split='train', batch_size=32, **data_args):

        self.batch_size = batch_size
        self.n_classes = n_classes

        modules = importlib.import_module('tensorflow.keras.datasets')

        if hasattr(modules, data_module):
            module = getattr(modules, data_module)
        else:
            raise ValueError("{} is not a valid dataset.".format(data_module))


        if split == 'train':
            (self._x, self._y), (_, _) = module.load_data(**data_args)
        else:
            (_, _), (self._x, self._y) = module.load_data(**data_args)


        if len(self._x.shape) == 3:
            self._x = np.expand_dims(self._x, -1)

        self._x = self._x / 255.
        self._y = K.utils.to_categorical(self._y, n_classes)
        self.ids = list(range(len(self._x)))

    def __getitem__(self, index):
        
        _from = index * self.batch_size
        slice = self.ids[_from:_from + self.batch_size] if _from + self.batch_size < len(self.ids) else self.ids[_from:]
        
        X, y = self._x[slice], self._y[slice]

        return X, y
        

    def __len__(self):
        """
        :return: Number of batches per epochs
        :rtype: int
        """
        
        return int(np.floor(len(self.ids) / self.batch_size))

